import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeSet;

public class Source {
	private static final List<Vertex> graph = new ArrayList<>();
	private static int time;

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int z = scanner.nextInt();
		while (z-- > 0) {
			VertexManager.clear();
			graph.clear();
			time = 0;
			int vertexCount = scanner.nextInt();
			for (int i = 1; i <= vertexCount; i++) {
				Vertex v = VertexManager.getVertex(i);
				int vertextDegree = scanner.nextInt();
				while (vertextDegree-- > 0) {
					v.neighbours.add(VertexManager.getVertex(scanner.nextInt()));
				}
				graph.add(v);
			}
			List<List<Vertex>> cc = connectedComponents();
			List<List<Vertex>> list = new ArrayList<>();
			for (List<Vertex> tmp : cc) {
				if (!tmp.isEmpty()) {
					list.add(tmp);
				}
			}
			StringBuilder connectedComponentsString = new StringBuilder(list.size() + ":");
			for (int i = 0; i < list.size() - 1; i++) {
				for (int j = i; j < list.size() - 1; j++) {
					if (list.get(j).get(0).name > list.get(j + 1).get(0).name) {
						List<Vertex> tmp = list.get(j);
						list.set(j, list.get(j + 1));
						list.set(j + 1, tmp);
					}
				}
			}
			for (List<Vertex> tmp : list) {
				StringBuilder s = new StringBuilder();
				for (Vertex v : tmp) {
					s.append(",").append(v);
				}
				connectedComponentsString.append(s.substring(1)).append(";");
			}
			List<String> bridgess = bridges();

			StringBuilder bridgesString = new StringBuilder();
			bridgesString.append(bridgess.size()).append(":");
			for (String s : bridgess) {
				bridgesString.append(s).append(";");
			}
			StringBuilder articulationPointsString = new StringBuilder();
			List<Integer> ap = articulationPoints();
			articulationPointsString.append(ap.size()).append(":");
			for (int i : ap) {
				articulationPointsString.append(i).append(";");
			}
			List<List<Edge>> bcc = biconnectedComponents();
			StringBuilder biconnectedComponentsString = new StringBuilder(bcc.size() + ":");
			List<List<Vertex>> result = new ArrayList<>();
			for (List<Edge> tmp : bcc) {
				Set<Vertex> s = new TreeSet<>();
				for (Edge e : tmp) {
					s.add(e.left);
					s.add(e.right);
				}
				List<Vertex> l = new ArrayList<>(s);
				result.add(l);
			}
			result.sort(Comparator.comparingInt(l -> l.get(0).name));
			for (List<Vertex> tmp : result) {
				StringBuilder s = new StringBuilder();
				for (Vertex v : tmp) {
					s.append(",").append(v);
				}
				biconnectedComponentsString.append(s.substring(1)).append(";");
			}
			System.out.println(connectedComponentsString);
			System.out.println(biconnectedComponentsString);
			System.out.println(bridgesString);
			System.out.println(articulationPointsString);
		}
		System.out.println();
		scanner.close();
	}

	private static void connectedComponentsUtil(Vertex u, int[] visited, List<List<Vertex>> result, int c) {
		visited[u.name - 1] = c;
		for (Vertex v : u.neighbours) {
			if (visited[v.name - 1] == 0) {
				visited[v.name - 1] = c;
				connectedComponentsUtil(v, visited, result, c);
			}
		}
	}

	private static List<List<Vertex>> connectedComponents() {
		List<List<Vertex>> result = new ArrayList<>();
		int c = 1;
		int[] visited = new int[graph.size()];
		for (Vertex v : graph) {
			if (visited[v.name - 1] == 0) {
				connectedComponentsUtil(v, visited, result, c++);
			}
		}
		for (int i = 0; i < c; i++) {
			result.add(new ArrayList<>());
		}
		for (Vertex v : graph) {
			result.get(visited[v.name - 1]).add(v);
		}
		return result;
	}

	private static void bridgesUtil(Vertex u, boolean[] visited, int[] disc, int[] low, int[] parent, List<String> pairs) {
		visited[u.name - 1] = true;
		disc[u.name - 1] = low[u.name - 1] = ++time;
		for (Vertex v : u.neighbours) {
			if (!visited[v.name - 1]) {
				parent[v.name - 1] = u.name - 1;
				bridgesUtil(v, visited, disc, low, parent, pairs);
				low[u.name - 1] = Math.min(low[u.name - 1], low[v.name - 1]);
				if (low[v.name - 1] > disc[u.name - 1]) {
					if (u.name < v.name) {
						pairs.add(u.name + "-" + v.name);
					} else {
						pairs.add(v.name + "-" + u.name);
					}
				}
			} else if (v.name - 1 != parent[u.name - 1]) {
				low[u.name - 1] = Math.min(low[u.name - 1], disc[v.name - 1]);
			}
		}
	}

	private static List<String> bridges() {
		boolean[] visited = new boolean[graph.size()];
		int[] disc = new int[graph.size()];
		int[] low = new int[graph.size()];
		int[] parent = new int[graph.size()];
		List<String> pairs = new ArrayList<>();

		for (int i = 0; i < graph.size(); i++) {
			parent[i] = -1;
			visited[i] = false;
		}

		for (Vertex v : graph) {
			if (!visited[v.name - 1]) {
				bridgesUtil(v, visited, disc, low, parent, pairs);
			}
		}
		pairs.sort((s1, s2) -> {
			int a1 = Integer.parseInt(s1.substring(0, s1.lastIndexOf('-')));
			int b1 = Integer.parseInt(s2.substring(0, s2.lastIndexOf('-')));
			int a2 = Integer.parseInt(s1.substring(s1.lastIndexOf('-') + 1));
			int b2 = Integer.parseInt(s2.substring(s2.lastIndexOf('-') + 1));
			if (a1 > b1) {
				return 1;
			} else if (a1 < b1) {
				return -1;
			} else {
				return a2 - b2;
			}
		});
		return pairs;
	}

	private static void articulationPointsUtil(Vertex u, boolean[] visited, int[] disc, int[] low, int[] parent, boolean[] ap) {
		int children = 0;
		visited[u.name - 1] = true;
		disc[u.name - 1] = low[u.name - 1] = ++time;
		for (Vertex v : u.neighbours) {
			if (!visited[v.name - 1]) {
				children++;
				parent[v.name - 1] = u.name - 1;
				articulationPointsUtil(v, visited, disc, low, parent, ap);
				low[u.name - 1] = Math.min(low[u.name - 1], low[v.name - 1]);
				if (parent[u.name - 1] == -1 && children > 1) {
					ap[u.name - 1] = true;
				}

				if (parent[u.name - 1] != -1 && low[v.name - 1] >= disc[u.name - 1]) {
					ap[u.name - 1] = true;
				}
			} else if (v.name - 1 != parent[u.name - 1]) {
				low[u.name - 1] = Math.min(low[u.name - 1], disc[v.name - 1]);
			}
		}
	}

	private static List<Integer> articulationPoints() {
		boolean[] visited = new boolean[graph.size()];
		int[] disc = new int[graph.size()];
		int[] low = new int[graph.size()];
		int[] parent = new int[graph.size()];
		boolean[] ap = new boolean[graph.size()];
		List<Integer> result = new ArrayList<>();
		for (Vertex v : graph) {
			parent[v.name - 1] = -1;
			visited[v.name - 1] = false;
			ap[v.name - 1] = false;
		}
		for (Vertex v : graph) {
			if (!visited[v.name - 1]) {
				articulationPointsUtil(v, visited, disc, low, parent, ap);
			}
		}

		for (Vertex v : graph) {
			if (ap[v.name - 1]) {
				result.add(v.name);
			}
		}
		Collections.sort(result);
		return result;
	}

	private static void biconnectedComponentsUtil(Vertex u, int[] disc, int[] low, LinkedList<Edge> st, int[] parent, List<List<Edge>> result) {
		disc[u.name - 1] = low[u.name - 1] = ++time;
		int children = 0;
		for (Vertex v : u.neighbours) {
			if (disc[v.name - 1] == -1) {
				children++;
				parent[v.name - 1] = u.name - 1;
				st.add(new Edge(u, v));
				biconnectedComponentsUtil(v, disc, low, st, parent, result);
				if (low[u.name - 1] > low[v.name - 1]) {
					low[u.name - 1] = low[v.name - 1];
				}

				if (disc[u.name - 1] == 1 && children > 1 || disc[u.name - 1] > 1 && low[v.name - 1] >= disc[u.name - 1]) {
					List<Edge> list = new ArrayList<>();
					while (st.getLast().left != u || st.getLast().right != v) {
						list.add(st.getLast());
						st.removeLast();
					}
					list.add(st.getLast());
					st.removeLast();
					if (list.size() > 1) {
						result.add(list);
					}
				}
			} else if (v.name - 1 != parent[u.name - 1] && disc[v.name - 1] < low[u.name - 1]) {
				if (low[u.name - 1] > disc[v.name - 1]) {
					low[u.name - 1] = disc[v.name - 1];
				}
				st.add(new Edge(u, v));
			}
		}
	}

	private static List<List<Edge>> biconnectedComponents() {
		List<List<Edge>> result = new ArrayList<>();
		int[] disc = new int[graph.size()];
		int[] low = new int[graph.size()];
		int[] parent = new int[graph.size()];
		LinkedList<Edge> st = new LinkedList<>();

		for (int i = 0; i < graph.size(); i++) {
			disc[i] = -1;
			low[i] = -1;
			parent[i] = -1;
		}

		for (Vertex v : graph) {
			if (disc[v.name - 1] == -1) {
				biconnectedComponentsUtil(v, disc, low, st, parent, result);

			}
			int j = 0;
			List<Edge> list = new ArrayList<>();
			while (!st.isEmpty()) {
				j = 1;
				list.add(st.getLast());
				st.removeLast();
			}
			if (list.size() > 1) {
				result.add(list);
			}
			if (j == 1) {
				System.out.println();
			}
		}
		return result;
	}
}

class Edge {
	Vertex left;
	Vertex right;

	Edge(Vertex left, Vertex right) {
		this.left = left;
		this.right = right;
	}

	@Override
	public String toString() {
		return left + "-" + right;
	}
}

class Vertex implements Comparable<Vertex> {
	final int name;
	final List<Vertex> neighbours = new ArrayList<>();

	Vertex(int name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return String.valueOf(name);
	}

	@Override
	public int compareTo(Vertex o) {
		return Objects.compare(name, o.name, Integer::compare);
	}
}

final class VertexManager {
	private static final Map<Integer, Vertex> vertexes = new HashMap<>();

	private VertexManager() {
	}

	static Vertex getVertex(int name) {
		if (vertexes.containsKey(name)) {
			return vertexes.get(name);
		}
		Vertex v = new Vertex(name);
		vertexes.put(name, v);
		return v;
	}

	static void clear() {
		vertexes.clear();
	}
}
